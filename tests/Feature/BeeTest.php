<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BeeTest extends TestCase
{
    /** @test */
    public function check_if_success_with_valid_data_on_store()
    {
        $data = [
            "name"      =>  "bee one",
            "specie"    =>  "specie"
        ];

        $response = $this->json('POST', 'api/bee', $data);

        $response
            ->assertStatus(200)
            ->assertJson([ 'success' => true ]);

        $this->assertDatabaseHas('bees', $data);
    }

    /** @test */
    public function check_if_error_with_invalid_data_on_store()
    {
        $data = [ "name"      =>  "bee two"];

        $response = $this->json('POST', 'api/bee', $data);

        $response
            ->assertStatus(400)
            ->assertJson([ 'success' => false ]);

        $this->assertDatabaseMissing('bees', $data);
    }

    /** @test */
    public function check_if_success_on_update()
    {
        $data = [
            'name'      => 'bee tree',
            'specie'    => 'other-specie'
        ];

        $response = $this->json('PUT', 'api/bee/1', $data);

        $response
            ->assertStatus(200)
            ->assertJson([ 'success' => true ]);

        $this->assertDatabaseHas('bees', $data);
    }

    /** @test */
    public function check_if_success_on_destroy()
    {
        $response = $this->json('DELETE', 'api/bee/1');

        $response
            ->assertStatus(200)
            ->assertJson([ 'success' => true ]);

        $this->assertSoftDeleted('bees', ['name' => 'bee tree']);	
    }
}
