<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Flower;

class FlowerTest extends TestCase
{
    /** @test */
    public function check_if_success_with_valid_data_on_store()
    {
        $data = [
            'name'          => 'flower one',
            'specie'        => 'flower-specie',
            'description'   => 'Lorem Ipsum',
            'months'        => ['jan', 'fev']
        ];

        $response = $this->json('POST', 'api/flower', $data);

        $response
            ->assertStatus(200)
            ->assertJson([ 'success' => true ]);

        $data['months'] = json_encode($data['months']);

        $this->assertDatabaseHas('flowers', $data);
    }

    /** @test */
    public function check_if_error_with_invalid_data_on_store()
    {
        $data = [
            'name'      => 'flower two',
            'specie'    => 'flower-specie',
        ];

        $response = $this->json('POST', 'api/flower', $data);

        $response
            ->assertStatus(400)
            ->assertJson([ 'success' => false ]);

        $this->assertDatabaseMissing('flowers', $data);
    }

    /** @test */
    public function check_if_success_on_update()
    {
        $data = [
            'name'          => 'flower updated',
            'specie'        => 'flower-other-specie',
            'description'   => 'Ipsum Lorem',
            'months'        => ['jan', 'march']
        ];

        $response = $this->json('PUT', 'api/flower/5', $data);

        $response
            ->assertStatus(200)
            ->assertJson([ 'success' => true ]);

        $data['months'] = json_encode($data['months']);

        $this->assertDatabaseHas('flowers', $data);
    }

    /** @test */
    public function check_if_success_on_destroy()
    {
        $response = $this->json('DELETE', 'api/flower/5');

        $response
            ->assertStatus(200)
            ->assertJson([ 'success' => true ]);

        $this->assertSoftDeleted('flowers', ['name' => 'flower updated']);	
    }

    /** @test */
    public function check_pivot_on_store()
    {
        $data = [
            'name'          => 'flower four',
            'specie'        => 'flower-specie',
            'description'   => 'Lorem Ipsum',
            'months'        => ['jan', 'fev'],
            'bees'          => [1, 2]
        ];

        $response = $this->json('POST', 'api/flower', $data);
   
        foreach ($data['bees'] as $beeId) {
            $this->assertDatabaseHas('bee_flower', [
                'flower_id' => $response['resource']['id'],
                'bee_id'    => $beeId
            ]);
        }

        $response = $this->json('DELETE', 'api/flower/6');

        $response
            ->assertStatus(200);
    }

    /** @test */
    public function check_if_response_is_correct_without_filters()
    {
        $response = $this->json('POST', 'api/search', array());
        
        $response
        ->assertStatus(200)
        ->assertJson([ 'success' => true ]);
        
        $flowersInResponse = $response['resource'];

        $allFlowers = Flower::all()->toArray();

        $this->assertEquals($flowersInResponse, $allFlowers);
    }

    /** @test */
    public function check_if_response_is_correct_with_bee_filter()
    {
        $data = ['bees' => array(3)];

        $response = $this->json('POST', 'api/search', $data);
        
        $response
        ->assertStatus(200)
        ->assertJson([ 'success' => true ]);

        $flowersInResponse = $response['resource'];

        $flowers = Flower::whereHas('bees', function($query) use ($data) {
            return $query->whereIn('bees.id', $data['bees']);
        })->get()->toArray();

        $this->assertEquals($flowersInResponse, $flowers);
    }

    /** @test */
    public function check_if_response_is_correct_with_month_filter()
    {
        $data = ['months' => array('oct')];

        $response = $this->json('POST', 'api/search', $data);
        
        $response
        ->assertStatus(200)
        ->assertJson([ 'success' => true ]);

        $flowersInResponse = $response['resource'];

        $month = $data['months'][0];

        $flowers = Flower::where('months', 'like', "%${month}%")->get()->toArray();
        
        $this->assertEquals(array_values($flowersInResponse), array_values($flowers ));
    }

    /** @test */
    public function check_if_response_is_correct_with_all_filter()
    {
        $data = ['months' => array('fev'), 'bees' => array(3)];

        $response = $this->json('POST', 'api/search', $data);
        
        $response
        ->assertStatus(200)
        ->assertJson([ 'success' => true ]);

        $flowersInResponse = $response['resource'];

        $month = $data['months'][0];

        $flowers = Flower::whereHas('bees', function($query) use ($data) {
            return $query->whereIn('bees.id', $data['bees']);
        });

        $flowers = $flowers->where('months', 'like', "%${month}%")->get()->toArray();
        
        $this->assertEquals(array_values($flowersInResponse), array_values($flowers ));
    }
}
