# Teste de PHP

Welcome to the Source.

## Setup de Teste

### Primeiros passos

É necessário instalar o Docker e docker-compose que sejam compatíveis com a versão 3 do docker-compose.

Clone o repositorio

```
git clone https://gitlab.com/marcelonascimento.contato/beeproject.git
```

(Caso seu S.O for linux execute todos os comandos abaixo como root)

Logo após clonar o repositorio acesse a pasta do projeto e levante o container.

```
cd beeProject
docker-compose up --d --b
```

Em seguida rode os seguintes comandos
```
docker exec -it beeproject_app_1 bash
./init.sh
```

Pronto! Seu ambiente foi configurado com sucesso.

Para rodar os testes execute os seguintes comandos
```
./runTest.sh
```
