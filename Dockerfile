FROM wyveo/nginx-php-fpm:php74

RUN apt-get update

RUN apt-get install curl

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash

RUN apt-get install nodejs

RUN node -v

RUN npm -v

RUN apt-get update

RUN apt-get install php-sqlite3 --assume-yes

WORKDIR /usr/share/nginx/

RUN rm -rf /usr/share/nginx/html

COPY . /usr/share/nginx

RUN chmod -R 777 storage/

RUN chmod +x init.sh

RUN chmod +x runTest.sh

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

ARG USER_ID=1000
ARG GROUP_ID=1000

RUN usermod -u $USER_ID www-data
RUN groupmod -g $GROUP_ID www-data