#!/bin/sh

set -e

composer install
touch database/test.sqlite
php artisan migrate:fresh --seed
