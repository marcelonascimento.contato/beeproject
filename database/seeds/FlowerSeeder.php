<?php

use Illuminate\Database\Seeder;

use App\Models\Flower;
use App\Models\BeeFlower;

class FlowerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            [ 'name' => 'Azaleia', 'specie' => 'flower-specie', 'months' => ['jan', 'fev'], 'beesId' => array(1, 2, 3) ],
            [ 'name' => 'Laranjeira', 'specie' => 'flower-specie', 'months' => ['jan', 'fev'], 'beesId' => array(1, 2) ],
            [ 'name' => 'Oregano', 'specie' => 'flower-specie', 'months' => ['march', 'oct'], 'beesId' => array(3) ],
            [ 'name' => 'Manjericao', 'specie' => 'flower-specie', 'months' => ['oct', 'jan'], 'beesId' => array(2) ],
        ];

        foreach ($array as $data) {
            $flower = Flower::Create([
                'name' => $data['name'], 
                'specie' => $data['specie'],
                'description' => 'Lorem Ipsum',
                'months' => $data['months'],
            ]);


            foreach ($data['beesId'] as $beeId) {
                $beeFlower = BeeFlower::Create([
                    'flower_id' => $flower->id,
                    'bee_id'    => $beeId
                ]);
            }
        }
    }
}
