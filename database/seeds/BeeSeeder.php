<?php

use Illuminate\Database\Seeder;

use App\Models\Bee;

class BeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Bee::updateOrCreate(['name' => 'Uruçu', 'specie' => 'Melipona scutellaris']);
        Bee::updateOrCreate(['name' => 'Uruçu-Amarela', 'specie' => 'Melipona rufiventris']);
        Bee::updateOrCreate(['name' => 'Guarupu', 'specie' => 'Melipona bicolor']);
        Bee::updateOrCreate(['name' => 'Iraí', 'specie' => 'Nannotrigona testaceicornes']);
    }
}
