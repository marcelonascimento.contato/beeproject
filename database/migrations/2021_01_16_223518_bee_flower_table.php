<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BeeFlowerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bee_flower', function (Blueprint $table) {
            $table->id();
            $table->foreignId('flower_id');
            $table->foreignId('bee_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('flower_id')->references('id')->on('flowers')->onDelete('cascade');
            $table->foreign('bee_id')->references('id')->on('bees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bee_flower_table');
    }
}
