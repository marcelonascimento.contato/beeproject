#!/bin/sh

set -e

php artisan migrate:fresh --env=testing --seed
./vendor/bin/phpunit --stop-on-failure
