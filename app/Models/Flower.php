<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flower extends Model
{
    use SoftDeletes;
    
    public function setMonthsAttribute($value)
    {
        $this->attributes['months'] = json_encode($value);
    }

    public function getMonthsAttribute($value)
    {
        return json_decode($value);
    }

    public function bees()
    {
        return $this->belongsToMany('App\Models\Bee', 'App\Models\BeeFlower', 'flower_id', 'bee_id');
    }

}
