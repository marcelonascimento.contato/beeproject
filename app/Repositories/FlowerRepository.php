<?php

namespace App\Repositories;

use App\Models\Flower;

class FlowerRepository
{
    public function __construct(Flower $model) {
        $this->model = new Flower;
    }

    public function filter($filters)
    {
        if(isset($filters['bees'])) {
            $flowers = $this->model->whereHas('bees', function($query) use ($filters) {
                return $query->whereIn('bees.id', $filters['bees']);
            })->get()->toArray();
        } else {
            $flowers = $this->model->all()->toArray();
        }

        if(isset($filters['months'])) {
            $flowers = array_filter($flowers, function($flower) use($filters) {
                    return !empty(array_intersect($flower['months'], $filters['months']));
            });
        }

        return $flowers;
    }

    public function getAll(): Object
    {
        return $this->model->with('bees')->get();
    }

    public function find(Int $id): Object
    {
        return $this->model->with('bees')->find($id);
    }

    public function delete(Int $id): Object
    {
        $model = $this->find($id);

        $model->delete();

        return $model;
    }

    public function store(Array $inputs): Object
    {
        return $this->save($this->model, $inputs);
    }

    public function update(Int $id, Array $inputs): Object
    {
        $model = $this->find($id);

        return $this->save($model, $inputs);
    }

    private function save(Flower $model, Array $inputs): Object
    {
        if(isset($inputs['name'])) {
            $model->name = $inputs['name'];
        }

        if(isset($inputs['specie'])) {
            $model->specie = $inputs['specie'];
        }

        if(isset($inputs['description'])) {
            $model->description = $inputs['description'];
        }

        if(isset($inputs['months'])) {
            $model->months = $inputs['months'];
        }
        
        $model->save();
        
        if(isset($inputs['bees'])) {
            $model->bees()->sync($inputs['bees']);
        }

        return $model;
    }
}