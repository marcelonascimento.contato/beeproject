<?php

namespace App\Repositories;

use App\Models\Bee;

class BeeRepository
{
    public function __construct(Bee $model) {
        $this->model = new Bee;
    }

    public function getAll(): Object
    {
        return $this->model->all();
    }

    public function find(Int $id): Object
    {
        return $this->model->find($id);
    }

    public function delete(Int $id): Object
    {
        $model = $this->find($id);

        $model->delete();

        return $model;
    }

    public function store(Array $inputs): Object
    {
        return $this->save($this->model, $inputs);
    }

    public function update(Int $id, Array $inputs): Object
    {
        $model = $this->find($id);

        return $this->save($model, $inputs);
    }

    private function save(Bee $model, Array $inputs): Object
    {
        if(isset($inputs['name'])) {
            $model->name = $inputs['name'];
        }

        if(isset($inputs['specie'])) {
            $model->specie = $inputs['specie'];
        }

        $model->save();

        return $model;
    }
}