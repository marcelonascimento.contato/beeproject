<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Models\Bee;
use App\Repositories\BeeRepository;

class BeeController extends Controller
{
    public function __construct(
        BeeRepository $BeeRepository
    ) {
        $this->BeeRepository = $BeeRepository;
    }

    public function index()
    {
        try {
            $success = true;
            $bee = $this->BeeRepository->getAll();
            
        } catch (\Throwable $th) {
            $success = false;
        }

        return Response()->json([
            'success'   => $success,
            'resources' => $bee
        ], $success ? 200 : 400);
    }

    public function store(Request $request)
    {
        try {
            $success = true;
            
            DB::beginTransaction();
            $inputs = $request->json()->all();

            $bee = $this->BeeRepository->store($inputs);
            
            DB::commit();
        } catch (\Throwable $th) {
            $success = false;
            DB::rollback();
        }

        return Response()->Json([
            'success'   => $success,
            'resource'  => $bee ?? null
        ], $success ? 200 : 400);


    }

    public function show($beeId)
    {
        $success = true;

        try {
            $bee = $this->BeeRepository->find($beeId);

        } catch (\Throwable $th) {
            $success = false;
        }
        return Response()->Json([
            'success'   => $success,
            'resource'  => $bee ?? null
        ], $success ? 200 : 400);
    }

    public function update(Request $request, $beeId)
    {
        try {
            $success = true;

            DB::beginTransaction();

            $inputs = $request->json()->all();

            $bee = $this->BeeRepository->update($beeId, $inputs);

            DB::commit();
        } catch (\Throwable $th) {
            $success = false;
            DB::rollback();;
        }        

        return Response()->Json([
            'success'   => $success,
            'resource'  => $bee ?? null
        ], $success ? 200 : 400);
    }

    public function destroy($beeId)
    {
        try {
            $success = true;

            DB::beginTransaction();

            $bee = $this->BeeRepository->delete($beeId);

            DB::commit();
        } catch (\Throwable $th) {
            $success = false;
            DB::rollback();
        }

        return Response()->Json([
            'success'   => $success,
            'resource'  => $bee ?? null
        ], $success ? 200 : 400);
    }
}
