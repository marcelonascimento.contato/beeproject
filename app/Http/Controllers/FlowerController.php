<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Models\Flower;
use App\Repositories\FlowerRepository;

class FlowerController extends Controller
{
    public function __construct(
        FlowerRepository $FlowerRepository
    ) {
        $this->FlowerRepository = $FlowerRepository;
    }

    public function search(Request $request)
    {
        $success = true;
        try {
            $inputs = $request->json()->all();

            $flowers = $this->FlowerRepository->filter($inputs);

        } catch (\Throwable $th) {
            $success = false;
        }

        return Response()->Json([
            'success' => $success,
            'resource' => $flowers ?? null
        ], $success ? 200 : 400);
    }

    public function index()
    {
        try {
            $success = true;
            $flowers = $this->FlowerRepository->getAll();
            
        } catch (\Throwable $th) {
            $success = false;
        }

        return Response()->json([
            'success'   => $success,
            'resources' => $flowers ?? null
        ], $success ? 200 : 400);
    }

    public function store(Request $request)
    {
        try {
            $success = true;
            
            DB::beginTransaction();
            $inputs = $request->json()->all();

            $flower = $this->FlowerRepository->store($inputs);

            DB::commit();
        } catch (\Throwable $th) {
            $success = false;
            DB::rollback();
        }

        return Response()->Json([
            'success'   => $success,
            'resource'  => $flower ?? null
        ], $success ? 200 : 400);


    }

    public function show($flowerId)
    {
        $success = true;

        try {
            $flower = $this->FlowerRepository->find($flowerId);
        } catch (\Throwable $th) {
            $success = false;
        }

        return Response()->Json([
            'success'   => $success,
            'resource'  => $flower ?? null
        ], $success ? 200 : 400);
    }

    public function update(Request $request, $flowerId)
    {
        try {
            $success = true;

            DB::beginTransaction();

            $inputs = $request->json()->all();

            $flower = $this->FlowerRepository->update($flowerId, $inputs);

            DB::commit();

            $flower = $this->FlowerRepository->find($flowerId);

        } catch (\Throwable $th) {
            $success = false;

            DB::rollback();
        }        

        return Response()->Json([
            'success'   => $success,
            'resource'  => $flower ?? null
        ], $success ? 200 : 400);
    }

    public function destroy($flowerId)
    {
        try {
            $success = true;

            DB::beginTransaction();

            $flower = $this->FlowerRepository->delete($flowerId);

            DB::commit();
        } catch (\Throwable $th) {
            $success = false;
            DB::rollback();
        }

        return Response()->Json([
            'success'   => $success,
            'resource'  => $flower ?? null
        ], $success ? 200 : 400);
    }
}
